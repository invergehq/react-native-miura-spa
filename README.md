# react-native-miura-spa

React native module for the Miura SPA SDK

## Installation

```sh
npm install react-native-miura-spa
```

## Usage

```js
import MiuraSpa from "react-native-miura-spa";

// ...

const result = await MiuraSpa.searchBluetooth();
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
