import * as React from 'react';

import {
  NativeEventEmitter,
  StyleSheet,
  View,
  ScrollView,
  Text,
  NativeModules,
  TextInput,
  Switch,
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import MiuraSpa from 'react-native-miura-spa';
import Terminal from './components/Terminal';
import AppInput from './components/AppInput';
import AppButton from './components/AppButton';
import AppText from './components/AppText';
import RefundForm from './components/RefundForm';

export default function App() {
  const terminal = React.useRef<React.ElementRef<typeof Terminal>>(null);
  // const [terminal, setTerminal] = React.useState<Element>((
  //   <Terminal handleClearMessages={() => {}} />
  // ));
  const [messages, setMessages] = React.useState<string[]>([]);
  const [devices, setDevices] = React.useState<
    | {
        id: string;
        name: string;
      }[]
    | undefined
  >([]);
  const [initialized, setInitialized] = React.useState<boolean>(false);
  const [deviceConnected, setDeviceConnected] = React.useState<boolean>(false);

  const [paramFile, setParamFile] = React.useState<string>(
    '/storage/emulated/0/Download/Params.txt'
  );

  const [displayMessage, setDisplayMessage] = React.useState<string>('');
  const [testMode, setTestMode] = React.useState<boolean>(false);
  const [selectedEntryMode, setSelectedEntryMode] = React.useState<
    'manual' | 'swipe' | 'contact' | 'contactless'
  >('manual');

  const [amount, setAmount] = React.useState<string>('10');
  const [cashbackAmount, setCashbackAmount] = React.useState<string>('');
  const [tipAmount, setTipAmount] = React.useState<string>('');

  const [manualIsCardPresent, setManualIsCardPresent] = React.useState<boolean>(
    false
  );
  const [manualCardAddress, setManualCardAddress] = React.useState<string>('');
  const [manualCardZipCode, setManualCardZipCode] = React.useState<string>('');

  const [
    miuraSpaEvent,
    setMiuraSpaEvent,
  ] = React.useState<NativeEventEmitter>();

  const handleSearchBluetooth = async () => {
    try {
      const result = await MiuraSpa.searchBluetooth();
      console.log('Search Complete');
      setDevices(result);
    } catch (error) {
      console.error(error);
      setMessages([...messages, error.toString()]);
    }
  };

  const handleConnectBluetoothPress = (deviceId: string) => async () => {
    try {
      const result = await MiuraSpa.connectBluetooth(deviceId);
      setDeviceConnected(result);
    } catch (error) {
      console.error(error);
      setMessages([...messages, error.toString()]);
    }
  };

  const handleInitialize = async () => {
    try {
      await MiuraSpa.initialize(paramFile);
      setInitialized(await MiuraSpa.getInitialized());
    } catch (error) {
      console.error(error);
      setMessages([...messages, error.toString()]);
    }
  };

  const handleSetDisplayMessage = async () => {
    try {
      await MiuraSpa.displayMessage(displayMessage);
    } catch (error) {
      console.error(error);
      setMessages([...messages, error.toString()]);
    }
  };

  const handleDisplayMessageChange = (value: string) =>
    setDisplayMessage(value);

  const handleSetTestMode = async (value: boolean) => {
    setTestMode(await MiuraSpa.setTestMode(value));
  };

  const handleExecuteSale = async () => {
    try {
      const result = await MiuraSpa.saleOperation(
        selectedEntryMode,
        {
          amount: parseFloat(amount || '0'),
          cashback: parseFloat(cashbackAmount || '0'),
          tip: parseFloat(tipAmount || '0'),
        },
        {
          cardPresent: manualIsCardPresent,
          address: manualCardAddress,
          postcode: manualCardZipCode,
        }
      );

      console.log(result);
    } catch (error) {
      console.error(error);
    }
  };

  const handleExecuteRefund = async (
    entryMode: 'manual' | 'swipe' | 'contact' | 'contactless',
    refund: {
      amount: number;
      originalTransactionId: string;
    },
    manualCard:
      | undefined
      | {
          cardPresent: boolean;
          address: string;
          postcode: string;
        }
  ) => {
    try {
      const result = await MiuraSpa.refundOperation(
        entryMode,
        refund,
        manualCard
      );

      console.log(result);

      return result;
    } catch (error) {
      console.error(error);
      return false;
    }
  };

  React.useEffect(() => {
    const onSendMessage = async (message: string) => {
      console.log('Send Message', message);
      if (terminal !== null) {
        if (terminal.current === undefined) {
          if (miuraSpaEvent)
            miuraSpaEvent.removeListener('onSendMessage', onSendMessage);
        } else terminal.current?.addMessage(message);
      }
    };

    if (!miuraSpaEvent) {
      console.log('init event');
      const nativeEvent = new NativeEventEmitter(NativeModules.MiuraSpa);
      setMiuraSpaEvent(nativeEvent);
      nativeEvent.addListener('onSendMessage', onSendMessage);
    }
  }, [miuraSpaEvent]);

  React.useEffect(() => {
    MiuraSpa.getTestMode()
      .then((currentTestMode) => {
        setTestMode(currentTestMode);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const renderSetDisplayTextForm = () => (
    <View style={styles.flexColumn}>
      <Text style={styles.label}>Set Display Text</Text>
      <AppInput>
        <TextInput
          style={styles.input}
          placeholder="Display message"
          value={displayMessage}
          onChangeText={handleDisplayMessageChange}
        />
      </AppInput>
      <AppButton
        style="info"
        onPress={handleSetDisplayMessage}
        title="Set Display Message"
      />
    </View>
  );

  const renderSaleForm = () => (
    <View>
      <Text style={styles.h1}>Sale Operation</Text>
      <View>
        <Text style={styles.label}>Test Mode</Text>
        <AppInput>
          <Switch
            style={styles.input}
            value={testMode}
            onValueChange={(value) => handleSetTestMode(value)}
          />
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Entry Mode</Text>
        <AppInput>
          <Picker
            style={styles.input}
            selectedValue={selectedEntryMode}
            onValueChange={(value) =>
              setSelectedEntryMode(
                value === 'manual' ||
                  value === 'swipe' ||
                  value === 'contact' ||
                  value === 'contactless'
                  ? value
                  : 'manual'
              )
            }
          >
            <Picker.Item label="Manual" value="manual" />
            <Picker.Item label="Swipe" value="swipe" />
            <Picker.Item label="Contact" value="contact" />
            <Picker.Item label="Contactless" value="contactless" />
          </Picker>
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Amount</Text>
        <AppInput>
          <TextInput
            style={styles.input}
            placeholder="0.00"
            value={amount}
            onChangeText={(value) => setAmount(value)}
          />
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Cashback Amount</Text>
        <AppInput>
          <TextInput
            style={styles.input}
            placeholder="0.00"
            value={cashbackAmount}
            onChangeText={(value) => setCashbackAmount(value)}
          />
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Tip Amount</Text>
        <AppInput>
          <TextInput
            style={styles.input}
            placeholder="0.00"
            value={tipAmount}
            onChangeText={(value) => setTipAmount(value)}
          />
        </AppInput>
      </View>
      {selectedEntryMode === 'manual' && (
        <>
          <View>
            <Text style={styles.label}>Card Is Present</Text>
            <AppInput>
              <Switch
                style={styles.input}
                value={manualIsCardPresent}
                onValueChange={(value) => setManualIsCardPresent(value)}
              />
            </AppInput>
          </View>
          <View>
            <Text style={styles.label}>Address</Text>
            <AppInput>
              <TextInput
                style={styles.input}
                placeholder="Some Street 123"
                value={manualCardAddress}
                onChangeText={(value) => setManualCardAddress(value)}
              />
            </AppInput>
          </View>
          <View>
            <Text style={styles.label}>Zip Code</Text>
            <AppInput>
              <TextInput
                style={styles.input}
                placeholder="ZIP"
                value={manualCardZipCode}
                onChangeText={(value) => setManualCardZipCode(value)}
              />
            </AppInput>
          </View>
        </>
      )}
      <AppButton
        style="success"
        onPress={handleExecuteSale}
        title="Execute Sale"
      />
    </View>
  );

  return (
    <View style={styles.container}>
      <View style={styles.layout}>
        <Terminal ref={terminal} />
        <ScrollView style={styles.interfaceContainer}>
          {deviceConnected ? (
            <>
              {initialized ? (
                <>
                  <AppButton
                    style="info"
                    onPress={handleInitialize}
                    title="Reinitialize Parameters"
                  />
                  <View style={styles.divider} />
                  {renderSetDisplayTextForm()}
                  <View style={styles.divider} />
                  {renderSaleForm()}
                  <View style={styles.divider} />
                  <RefundForm onFormSubmit={handleExecuteRefund} />
                </>
              ) : (
                <>
                  <View style={styles.flexColumn}>
                    <Text style={styles.label}>
                      Set Initialization Param File
                    </Text>
                    <AppInput>
                      <TextInput
                        style={styles.input}
                        placeholder="Display path"
                        value={paramFile}
                        onChangeText={(value) => {
                          setParamFile(value);
                        }}
                      />
                    </AppInput>
                    <AppButton
                      style="info"
                      onPress={handleInitialize}
                      title="Initialize Parameters"
                    />
                  </View>
                </>
              )}
            </>
          ) : (
            <>
              <AppButton
                style="info"
                onPress={handleSearchBluetooth}
                title="Search Bluetooth Devices"
              />
              <View style={styles.divider} />
              <ScrollView>
                {devices && devices.length ? (
                  devices.map(({ id, name }) => (
                    <View key={id} style={styles.row}>
                      <AppText>
                        <Text>{name}</Text>
                      </AppText>
                      <AppButton
                        style="success"
                        onPress={handleConnectBluetoothPress(id)}
                        title="Connect"
                      />
                    </View>
                  ))
                ) : (
                  <AppText>
                    <Text>No devices</Text>
                  </AppText>
                )}
              </ScrollView>
            </>
          )}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  layout: {
    flex: 1,
    flexDirection: 'row',
  },
  box: {
    width: 60,
    height: 60,
    marginVertical: 20,
  },
  interfaceContainer: {
    flex: 1,
    flexDirection: 'column',
  },
  divider: {
    borderStyle: 'solid',
    borderColor: '#eee',
    borderTopWidth: 1,
    margin: 8,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  flexColumn: {
    flexDirection: 'column',
  },
  flexGrow: {
    flexGrow: 1,
  },
  h1: {
    margin: 8,
    fontSize: 24,
    fontWeight: 'bold',
  },
  label: {
    margin: 8,
    fontWeight: 'bold',
  },
  input: {
    margin: 8,
    paddingHorizontal: 8,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
  },
});
