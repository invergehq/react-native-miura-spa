import React from 'react';

import { View, Button, ButtonProps, StyleSheet } from 'react-native';
import { colors } from '../constants';

interface AppButtonProps extends ButtonProps {
  style:
    | 'primary'
    | 'secondary'
    | 'success'
    | 'danger'
    | 'warning'
    | 'info'
    | 'light'
    | 'dark'
    | 'muted'
    | 'white';
}

const AppButton = (props: AppButtonProps) => (
  <View style={styles.button}>
    <Button
      color={colors.background[props.style]}
      onPress={props.onPress}
      title={props.title}
    />
  </View>
);

const styles = StyleSheet.create({
  button: {
    margin: 8,
  },
});

export default AppButton;
