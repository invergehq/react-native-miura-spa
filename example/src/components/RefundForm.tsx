import * as React from 'react';

import { View, Text, TextInput, Switch } from 'react-native';
import { Picker } from '@react-native-picker/picker';

import AppInput from './AppInput';
import AppButton from './AppButton';
import { StyleSheet } from 'react-native';

interface RefundFormProps {
  onFormSubmit: (
    entryMode: 'manual' | 'swipe' | 'contact' | 'contactless',
    refund: {
      amount: number;
      originalTransactionId: string;
    },
    manualCard:
      | undefined
      | {
          cardPresent: boolean;
          address: string;
          postcode: string;
        }
  ) => Promise<boolean>;
}

const RefundForm = (props: RefundFormProps) => {
  const { onFormSubmit } = props;

  const [selectedEntryMode, setSelectedEntryMode] = React.useState<
    'manual' | 'swipe' | 'contact' | 'contactless'
  >('manual');
  const [amount, setAmount] = React.useState<string>('10');
  const [transactionId, setTransactionId] = React.useState<string>('');

  const [manualIsCardPresent, setManualIsCardPresent] = React.useState<boolean>(
    false
  );
  const [manualCardAddress, setManualCardAddress] = React.useState<string>('');
  const [manualCardZipCode, setManualCardZipCode] = React.useState<string>('');

  const handleFormSubmit = () => {
    onFormSubmit(
      selectedEntryMode,
      {
        amount: parseFloat(amount || '0'),
        originalTransactionId: transactionId,
      },
      {
        cardPresent: manualIsCardPresent,
        address: manualCardAddress,
        postcode: manualCardZipCode,
      }
    );
  };

  return (
    <View>
      <Text style={styles.h1}>Refund Operation</Text>
      <View>
        <Text style={styles.label}>Entry Mode</Text>
        <AppInput>
          <Picker
            style={styles.input}
            selectedValue={selectedEntryMode}
            onValueChange={(value) =>
              setSelectedEntryMode(
                value === 'manual' ||
                  value === 'swipe' ||
                  value === 'contact' ||
                  value === 'contactless'
                  ? value
                  : 'manual'
              )
            }
          >
            <Picker.Item label="Manual" value="manual" />
            <Picker.Item label="Swipe" value="swipe" />
            <Picker.Item label="Contact" value="contact" />
            <Picker.Item label="Contactless" value="contactless" />
          </Picker>
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Amount</Text>
        <AppInput>
          <TextInput
            style={styles.input}
            placeholder="0.00"
            value={amount}
            onChangeText={(value) => setAmount(value)}
          />
        </AppInput>
      </View>
      <View>
        <Text style={styles.label}>Transaction ID</Text>
        <AppInput>
          <TextInput
            style={styles.input}
            value={transactionId}
            onChangeText={(value) => setTransactionId(value)}
          />
        </AppInput>
      </View>
      {selectedEntryMode === 'manual' && (
        <>
          <View>
            <Text style={styles.label}>Card Is Present</Text>
            <AppInput>
              <Switch
                style={styles.input}
                value={manualIsCardPresent}
                onValueChange={(value) => setManualIsCardPresent(value)}
              />
            </AppInput>
          </View>
          <View>
            <Text style={styles.label}>Address</Text>
            <AppInput>
              <TextInput
                style={styles.input}
                placeholder="Some Street 123"
                value={manualCardAddress}
                onChangeText={(value) => setManualCardAddress(value)}
              />
            </AppInput>
          </View>
          <View>
            <Text style={styles.label}>Zip Code</Text>
            <AppInput>
              <TextInput
                style={styles.input}
                placeholder="ZIP"
                value={manualCardZipCode}
                onChangeText={(value) => setManualCardZipCode(value)}
              />
            </AppInput>
          </View>
        </>
      )}
      <AppButton
        style="success"
        onPress={handleFormSubmit}
        title="Execute Refund"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  h1: {
    margin: 8,
    fontSize: 24,
    fontWeight: 'bold',
  },
  label: {
    margin: 8,
    fontWeight: 'bold',
  },
  input: {
    margin: 8,
    paddingHorizontal: 8,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
  },
});

export default RefundForm;
