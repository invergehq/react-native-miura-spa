import React from 'react';

import { View, ViewProps, StyleSheet } from 'react-native';

interface AppTextProps extends ViewProps {
  children?: Element;
}
const AppText = (props: AppTextProps) => (
  <View style={styles.text}>{props.children}</View>
);

const styles = StyleSheet.create({
  text: {
    margin: 8,
  },
});

export default AppText;
