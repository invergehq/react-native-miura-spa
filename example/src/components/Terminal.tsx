import React from 'react';

import { View, Button, ScrollView, StyleSheet, Text } from 'react-native';
import { colors } from '../constants';

type TerminalHandle = {
  addMessage: (message: string) => string;
};

type TerminalProps = {};

const Terminal = React.forwardRef<TerminalHandle, TerminalProps>(
  (props: any, ref: any) => {
    const [messages, setMessages] = React.useState<string[]>([]);

    React.useImperativeHandle(ref, () => ({
      addMessage: (message: string) => {
        console.log('Terminal Add Message', message);
        setMessages([message, ...messages]);
      },
    }));

    return (
      <View {...props} style={styles.terminalContainer}>
        <View style={styles.terminal}>
          <ScrollView style={styles.flexGrow}>
            {messages.map((message, index) => (
              <Text key={index} style={styles.terminalText}>
                &gt; {message}
              </Text>
            ))}
          </ScrollView>
          <Button
            onPress={() => {
              setMessages([]);
            }}
            title="Clear"
            color={colors.background.danger}
          />
        </View>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  terminalContainer: {
    flex: 2,
  },
  terminal: {
    flex: 1,
    backgroundColor: '#000',
  },
  terminalText: {
    color: 'white',
    fontSize: 20,
    fontFamily: 'monospace',
  },
  flexGrow: {
    flexGrow: 1,
  },
});

export default Terminal;
