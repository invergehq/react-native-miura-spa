import React from 'react';

import { View, ViewProps } from 'react-native';

interface AppInputProps extends ViewProps {
  children?: Element;
}
const AppInput = (props: AppInputProps) => <View>{props.children}</View>;

export default AppInput;
