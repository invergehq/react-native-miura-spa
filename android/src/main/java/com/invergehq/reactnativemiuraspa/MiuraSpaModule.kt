package com.invergehq.reactnativemiuraspa

import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat.startActivityForResult
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.bridge.ReactContextBaseJavaModule
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.ReactMethod
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.WritableNativeArray
import com.facebook.react.bridge.WritableNativeMap
import com.facebook.react.modules.core.DeviceEventManagerModule
import com.gemstonepay.gemware.platform.enums.CardEntryModesAllowed
import com.gemstonepay.gemware.platform.enums.GemwareParameters
import com.gemstonepay.gemware.platform.enums.Results
import com.gemstonepay.gemware.terminalConnectors.bluetooth.BluetoothConnector
import com.gemware.genericapi.common.BaseEvent
import com.gemware.genericapi.common.IGemwareEvents
import com.gemware.genericapi.common.enums.StartResultTypes
import com.gemware.genericapi.common.operations.DisplayOperation
import com.gemware.genericapi.common.operations.RefundOperation
import com.gemware.genericapi.common.operations.SaleOperation
import com.gemware.genericapi.common.operations.SetParametersOperation
import com.gemware.genericapi.common.responses.RefundResponse
import com.gemware.genericapi.common.responses.SaleResponse
import com.gemware.mpi.lib.GemwareMpi
import kotlinx.coroutines.*
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.coroutines.CoroutineContext

class MiuraSpaModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext), CoroutineScope, IGemwareEvents {
  private var job: Job = Job()

  override val coroutineContext: CoroutineContext
    get() = Dispatchers.Main + job

  private val gemwareApi = GemwareMpi()

  private val requestenable = 1

  private var testMode = false;

  private var initialized = false;

  init {
    Log.d("ReactNativeMiuraSPA", "init")
    this.gemwareApi.registerListener(this@MiuraSpaModule)
  }

  override fun getName(): String {
    return "MiuraSpa"
  }

  private fun sendMessage(message: String) {
    Log.d("ReactNativeMiuraSPA", message)
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onSendMessage", message)
  }

  private fun sendTerminalEvent(message: String) {
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onTerminalEvent", message)
  }

  @ReactMethod
  fun addListener(eventName: String) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  @ReactMethod
  fun removeListeners(count: Int) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  @ReactMethod
  fun getInitialized(promise: Promise) {
    promise.resolve(this.initialized);
  }

  @ReactMethod
  fun getTestMode(promise: Promise) {
    promise.resolve(this.testMode);
  }

  @ReactMethod
  fun searchBluetooth(promise: Promise) {
    sendMessage("Search Bluetooth Devices...")

    try {
      var devices = listBluetooth()

      var response = WritableNativeArray()

      for (device: BluetoothDevice in devices) {
        var entry = WritableNativeMap()
        entry.putString("id", device.uuids[0].uuid.toString())
        entry.putString("name", device.name)
        response.pushMap(entry)
      }

      sendMessage("Search Bluetooth Devices - Found " + devices.size + " devices")

      promise.resolve(response)
    } catch(e: Exception) {
      promise.reject(e);
    }
  }

  @ReactMethod
  fun connectBluetooth(uuid: String, promise: Promise) {
    sendMessage("Connect Bluetooth...")

    try {
      val list = listBluetooth()
      var notFound = true

      for (device: BluetoothDevice in list) {
        if (uuid == device.uuids[0].uuid.toString()) {
          launch {
            setBluetoothDevice(device, promise)
            sendMessage("Connect Bluetooth - Device connected")
          }
          notFound = false
          break
        }
      }

      if (notFound) {
        sendMessage("Connect Bluetooth - Device not found")
        promise.reject("Error", "Device not found")
      }
    } catch (e: Exception) {
      sendMessage("Connect Bluetooth - Connection failed")
      promise.reject(e)
    }
  }

  @ReactMethod
  fun initialize(path: String, promise: Promise) {
    sendMessage("Initialize Device...")

    val pathDown = path.toString()
    val lineList = mutableListOf<String>()

    if (File("$pathDown").exists()) {
      try {
        File("$pathDown").useLines { lines -> lines.forEach { lineList.add(it) } }
        lineList.forEach { println("$it") }

        val data = mutableMapOf<String, String>()
        var delimiter: String = "~"
        var keyList: MutableList<String> = mutableListOf()

        for (i in 0 until lineList.size step 1) {
          var subStr =
            lineList[i].substring(lineList[i].indexOf('~') + 1, lineList[i].length)
          val (key, value) = subStr.split(delimiter)
          if (key != "RFU") {
            data[key] = value
            keyList.add(key)
          }
        }

        for (i in 0 until data.size step 1) {
          if (keyList[i] == "HostTerminal") {
            var found = true
          }
          val set = SetParametersOperation().apply {
            set(keyList[i], data.get(keyList[i]).toString())
          }
          this.gemwareApi.setupOperation(set)

          launch {
            val operResp = set.executeAsync().await()
            if (operResp.result == Results.OK) {
              sendMessage(keyList[i] + "=" + data.get(keyList[i]).toString() + " Success " + operResp.result.toString())
            } else {
              sendMessage("Initialize Device - " + keyList[i] + ": Failed " + operResp.result.toString())
            }
          }
        }

        this.initialized = true;

        sendMessage("Initialize Device - Result success")
        promise.resolve(true)
      } catch (e: Exception) {
        this.initialized = false;

        sendMessage("Initialize Device - Result failed")
        promise.reject(e)
      }
    } else {
      this.initialized = false;
      sendMessage("The file doesn't exist")
      promise.reject("Error", "File not found")
    }
  }

  @ReactMethod
  fun displayMessage(message: String, promise: Promise) {
    sendMessage("Display Message...")
    sendMessage("Display Message - Message: $message")
    launch {
      setMessage(message, promise)
    }
  }

  @ReactMethod
  fun setTestMode(testMode: Boolean, promise: Promise) {
    sendMessage("Set Test Mode...")
    this.testMode = testMode
    this.gemwareApi.setTestMode(testMode)

    if(testMode) sendMessage("Set Test Mode - Enabled")
    else sendMessage("Set Test Mode - Disabled")

    promise.resolve(this.testMode)
  }

  @ReactMethod
  fun saleOperation(entryMode: String, transaction: ReadableMap, manualCard: ReadableMap, promise: Promise) {
    sendMessage("Sale Operation: $entryMode")

    var paramAmount = .0

    if (transaction.hasKey("amount")) paramAmount = transaction.getDouble("amount")
    else promise.reject("Error", "Must provide an amount value.")

    val paramCashbackAmount = if (transaction.hasKey("cashback")) transaction.getDouble("cashback") else .0
    val paramTipAmount = if (transaction.hasKey("tip")) transaction.getDouble("tip") else .0
    val paramReference = if (transaction.hasKey("reference")) transaction.getString("reference") else ""
    val paramCardPresent = if (manualCard.hasKey("cardPresent")) manualCard.getBoolean("cardPresent") else false
    val paramAddress = if (manualCard.hasKey("address")) manualCard.getString("address") else ""
    val paramZipCode = if (manualCard.hasKey("postcode")) manualCard.getString("postcode") else ""

    sendMessage("Sale Operation - Parameter Amount: $paramAmount")
    sendMessage("Sale Operation - Parameter Cashback Amount: $paramCashbackAmount")
    sendMessage("Sale Operation - Parameter Tip Amount: $paramTipAmount")
    sendMessage("Sale Operation - Parameter Reference: $paramReference")
    sendMessage("Sale Operation - Parameter Card Present?: $paramCardPresent")
    sendMessage("Sale Operation - Parameter Address: $paramAddress")
    sendMessage("Sale Operation - Parameter ZIP Code: $paramZipCode")

    if (!this.gemwareApi.isTerminalConnected()) {
      promise.reject("Error", "Terminal is not connected.")
    }

    if (this.gemwareApi.isBusy()) {
      promise.reject("Error", "Terminal is busy.")
    }

    this.gemwareApi.setTestMode(true)

    val sale = SaleOperation(paramAmount).apply {
      cashBackAmount = paramCashbackAmount
      tipAmount = paramTipAmount
      reference = paramReference
      manualData.isCardPresent = true

      when (entryMode) {
        "manual" -> cardEntryModeAllowed = CardEntryModesAllowed.Manual
        "swipe" -> cardEntryModeAllowed = CardEntryModesAllowed.Swipe
        "contact" -> cardEntryModeAllowed = CardEntryModesAllowed.Contact
        "contactless" -> cardEntryModeAllowed = CardEntryModesAllowed.Contactless
      }

      if (cardEntryModeAllowed == CardEntryModesAllowed.Manual) {
        manualData.isCardPresent = paramCardPresent
        manualData.address = paramAddress
        manualData.zip = paramZipCode
      }
    }

    val setBatchId = SetParametersOperation().apply {
      set(GemwareParameters.BatchID.value, "1")
    }

    this.gemwareApi.setupOperation(setBatchId)

    this.gemwareApi.setupOperation(sale)

    launch {
      val setBatchIdResponse = setBatchId.executeAsync().await()
      sendMessage("Set BatchID: " + setBatchIdResponse.result.toString())
      val saleResponse = sale.executeAsync().await()
      sendMessage("Sale Operation - Sale Response: " + saleResponse.result.toString())

      if (saleResponse.result == Results.OK) {
        if (saleResponse.approval.value == "approved") {
          promise.resolve(getSaleOperationResponse(saleResponse))
        } else {
          promise.reject("Transaction rejected", getSaleOperationResponse(saleResponse))
        }
      } else {
        promise.reject("Transaction rejected", getSaleOperationResponse(saleResponse))
      }
    }
  }

  @ReactMethod
  fun refundOperation(entryMode: String, refund: ReadableMap, manualCard: ReadableMap, promise: Promise) {
    sendMessage("Refund Operation: $entryMode")

    var paramAmount = .0
    var paramOriginalTransactionId = ""

    if (refund.hasKey("amount")) paramAmount = refund.getDouble("amount")
    else promise.reject("Error", "Must provide an amount value.")

    if (refund.hasKey("originalTransactionId")) paramOriginalTransactionId = refund.getString("originalTransactionId")!!
    else promise.reject("Error", "Must provide the original transaction ID.")

    val paramCardPresent = if (manualCard.hasKey("cardPresent")) manualCard.getBoolean("cardPresent") else false
    val paramAddress = if (manualCard.hasKey("address")) manualCard.getString("address") else ""
    val paramZipCode = if (manualCard.hasKey("postcode")) manualCard.getString("postcode") else ""

    sendMessage("Refund Operation - Parameter Amount: $paramAmount")
    sendMessage("Refund Operation - Parameter Original Transaction ID: $paramOriginalTransactionId")
    sendMessage("Refund Operation - Parameter Card Present?: $paramCardPresent")
    sendMessage("Refund Operation - Parameter Address: $paramAddress")
    sendMessage("Refund Operation - Parameter ZIP Code: $paramZipCode")

    if (!this.gemwareApi.isTerminalConnected()) {
      promise.reject("Error", "Terminal is not connected.")
    }

    if (this.gemwareApi.isBusy()) {
      promise.reject("Error", "Terminal is busy.")
    }

    this.gemwareApi.setTestMode(true)

    var cardEntryModeAllowed = CardEntryModesAllowed.Manual

    when (entryMode) {
      "Swipe" -> cardEntryModeAllowed = CardEntryModesAllowed.Swipe
      "Contact" -> cardEntryModeAllowed = CardEntryModesAllowed.Contact
      "Contactless" -> cardEntryModeAllowed = CardEntryModesAllowed.Contactless
    }

    val refundOperation = RefundOperation(paramAmount).apply {
      originalTransactionId = paramOriginalTransactionId
      manualData.isCardPresent = paramCardPresent

      when (entryMode) {
        "Manual" -> cardEntryModeAllowed = CardEntryModesAllowed.Manual
        "Swipe" -> cardEntryModeAllowed = CardEntryModesAllowed.Swipe
        "Contact" -> cardEntryModeAllowed = CardEntryModesAllowed.Contact
        "Contactless" -> cardEntryModeAllowed = CardEntryModesAllowed.Contactless
      }

      if (cardEntryModeAllowed == CardEntryModesAllowed.Manual) {
        manualData.address = paramAddress
        manualData.zip = paramZipCode
      }
    }

    this.gemwareApi.setupOperation(refundOperation)

    launch {
      val refundResponse = refundOperation.executeAsync().await()
      sendMessage("Refund Operation - Refund Response: " + refundResponse.result.toString())

      if (refundResponse.result == Results.OK) {
        if (refundResponse.approval.value == "approved") {
          promise.resolve(getRefundOperationResponse(refundResponse))
        } else {
          promise.reject("Refund rejected", getRefundOperationResponse(refundResponse))
        }
      } else {
        promise.reject("Refund rejected", getRefundOperationResponse(refundResponse))
      }
    }
  }

  private fun listBluetooth(): ArrayList<BluetoothDevice> {
    val pairedDevices: Set<BluetoothDevice>?

    val blueToothAdapter = BluetoothAdapter.getDefaultAdapter()
      ?: throw Exception("BlueToothAdapter is null")

    if (!blueToothAdapter.isEnabled) {
      val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
      val activity = this.reactApplicationContext.currentActivity
      if(activity != null) {
        startActivityForResult(activity, enableBluetoothIntent, requestenable, null)
      }
      sendMessage("Bluetooth Must Be Enabled")
    }

    pairedDevices = blueToothAdapter.bondedDevices
    val list: ArrayList<BluetoothDevice> = ArrayList()

    if ((pairedDevices as MutableSet<BluetoothDevice>?)?.isEmpty()!!)
      throw Exception("No paired devices")

    for (device: BluetoothDevice in pairedDevices!!) {
      if (device.name.contains("Miura")) {
        list.add(device)
      }
    }

    if (list.isEmpty())
      throw Exception("No paired miura devices")
    BluetoothConnector.THE_UUID = list[0].uuids[0].uuid
    return list
  }

  private suspend fun setParameter(parameter: String, value: String) {
    val setParametersOperation = SetParametersOperation().apply {
      set(parameter, value)
    }
    this.gemwareApi.setupOperation(setParametersOperation)
    val operResp = setParametersOperation.executeAsync().await()
    if (operResp.result == Results.OK) {
      sendMessage("Set parameter $parameter=$value")
    } else {
      sendMessage("Failed to set parameter $parameter=$value")
    }
  }

  private suspend fun setMessage(message: String, promise: Promise) {
    if (this.setMessage(message)) promise.resolve(true)
    else promise.reject("Failed", "Failed to set message")
  }

  private suspend fun setMessage(message: String): Boolean {
    val displayText = DisplayOperation("\n\u0002centre\u0003$message")
    this.gemwareApi.setupOperation(displayText)
    val status = displayText.executeAsync().await()
    sendMessage("Display Message - Success")
    return (status.result == Results.OK)
  }

  private suspend fun setBluetoothDevice(device: BluetoothDevice, promise: Promise) {
    var connected = false
    this.gemwareApi.initializeBluetooth(this.reactApplicationContext, device)

    val startResp = this.gemwareApi.startAsync().await()
    sendMessage(startResp.result.toString());
    sendMessage("Connect result " + startResp.result.toString());
    if (startResp.result == Results.OK) {
      sendMessage("Start result " + startResp.startSuccess.toString());
      if (startResp.startSuccess == StartResultTypes.Success) {
        connected = true
      }
    }

    if (connected) {
      sendMessage("Connected")
      this.gemwareApi.registerListener(this@MiuraSpaModule)
      promise.resolve(true)
    } else {
      sendMessage("Disconnected")
      promise.reject("Error", "Failed to connect")
    }
  }

  private fun getSaleOperationResponse(saleResponse: SaleResponse): WritableNativeMap {
    val response = WritableNativeMap()
    response.putString("AID", saleResponse.AID)
    response.putString("ARC", saleResponse.ARC)
    response.putString("IAD", saleResponse.IAD)
    response.putString("TSI", saleResponse.TSI)
    response.putString("TVR", saleResponse.TVR)
    response.putString("account", saleResponse.account)
    response.putString("approval", saleResponse.approval.value)
    response.putString("approvalMode", saleResponse.approvalMode)
    response.putString("authCode", saleResponse.authCode)
    response.putDouble("balance", saleResponse.balance ?: 0.00)
    response.putString("cardBrand", saleResponse.cardBrand)
    response.putDouble("cashBackAmount", saleResponse.cashBackAmount ?: 0.00)
    response.putString("clerkId", saleResponse.clerkId)
    response.putString("cvm", saleResponse.cvm)
    response.putString("dateTime", saleResponse.dateTime)
    response.putDouble("dueAmount", saleResponse.dueAmount ?: 0.00)
    response.putString("ebtApprovalCode", saleResponse.ebtApprovalCode)
    response.putString("ebtVoucher", saleResponse.ebtVoucher)
    response.putString("entryMode", saleResponse.entryMode)
    response.putString("hostMessage", saleResponse.hostMessage)
    response.putString("hostTransactionId", saleResponse.hostTransactionId)
    response.putString("invoice", saleResponse.invoice)
    response.putString("merchantID", saleResponse.merchantID)
    response.putString("preferredName", saleResponse.preferredName)
    response.putBoolean("printTipLine", saleResponse.printTipLine)

    val receiptData = WritableNativeMap();
    receiptData.putString("footerLine1", saleResponse.receiptData.footerLine1)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    receiptData.putString("footerLine3", saleResponse.receiptData.footerLine3)
    receiptData.putString("headerLine1", saleResponse.receiptData.headerLine1)
    receiptData.putString("headerLine2", saleResponse.receiptData.headerLine2)
    receiptData.putString("headerLine3", saleResponse.receiptData.headerLine3)
    receiptData.putString("headerLine4", saleResponse.receiptData.headerLine4)
    receiptData.putString("headerLine5", saleResponse.receiptData.headerLine5)
    receiptData.putString("processorLine1", saleResponse.receiptData.processorLine1)
    receiptData.putString("processorLine2", saleResponse.receiptData.processorLine2)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", saleResponse.receiptData.footerLine2)
    response.putMap("receiptData", receiptData)

    response.putString("reference", saleResponse.reference)
    response.putDouble("requestedAmount", saleResponse.requestedAmount ?: 0.00)
    response.putString("responseCode", saleResponse.responseCode)
    response.putInt("shift", saleResponse.shift ?: 0)
    response.putString("signatureData", saleResponse.signatureData)
    response.putBoolean("signatureRequired", saleResponse.signatureRequired)
    response.putInt("stan", saleResponse.stan)
    response.putString("storeID", saleResponse.storeID)
    response.putDouble("subTotalAmount", saleResponse.subTotalAmount)
    response.putDouble("surchargeAmount", saleResponse.surchargeAmount ?: 0.00)
    response.putDouble("surchargeFixed", saleResponse.surchargeFixed ?: 0.00)
    response.putDouble("surchargePercentage", saleResponse.surchargePercentage ?: 0.00)
    response.putInt("table", saleResponse.table ?: 0)
    response.putDouble("taxAmount", saleResponse.taxAmount ?: 0.00)
    response.putString("terminalID", saleResponse.terminalID)
    response.putDouble("tipAmount", saleResponse.tipAmount ?: 0.00)
    response.putDouble("totalAmount", saleResponse.totalAmount)
    response.putString("transactionId", saleResponse.transactionId)
    response.putInt("transactionType", saleResponse.transactionType.value)
    response.putString("result", saleResponse.result.value)

    sendMessage("Sale Operation - Response Result: " + response.getString("result"))
    sendMessage("Sale Operation - Response Approval: " + response.getString("approval"))
    sendMessage("Sale Operation - Response Host Message: " + response.getString("hostMessage"))
    sendMessage("Sale Operation - Transaction ID: " + saleResponse.transactionId)

    return response
  }

  private fun getRefundOperationResponse(refundResponse: RefundResponse): WritableNativeMap {
    val response = WritableNativeMap()
    response.putString("AID", refundResponse.AID)
    response.putString("ARC", refundResponse.ARC)
    response.putString("IAD", refundResponse.IAD)
    response.putString("TSI", refundResponse.TSI)
    response.putString("TVR", refundResponse.TVR)
    response.putString("account", refundResponse.account)
    response.putString("approval", refundResponse.approval.value)
    response.putString("authCode", refundResponse.authCode)
    response.putDouble("balance", refundResponse.balance ?: 0.00)
    response.putString("cardBrand", refundResponse.cardBrand)
    response.putDouble("cashBackAmount", refundResponse.cashBackAmount ?: 0.00)
    response.putString("clerkId", refundResponse.clerkId)
    response.putString("cvm", refundResponse.cvm)
    response.putString("dateTime", refundResponse.dateTime)

    response.putString("entryMode", refundResponse.entryMode)
    response.putString("hostMessage", refundResponse.hostMessage)
    response.putString("merchantID", refundResponse.merchantID)
    response.putString("preferredName", refundResponse.preferredName)

    val receiptData = WritableNativeMap();
    receiptData.putString("footerLine1", refundResponse.receiptData.footerLine1)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    receiptData.putString("footerLine3", refundResponse.receiptData.footerLine3)
    receiptData.putString("headerLine1", refundResponse.receiptData.headerLine1)
    receiptData.putString("headerLine2", refundResponse.receiptData.headerLine2)
    receiptData.putString("headerLine3", refundResponse.receiptData.headerLine3)
    receiptData.putString("headerLine4", refundResponse.receiptData.headerLine4)
    receiptData.putString("headerLine5", refundResponse.receiptData.headerLine5)
    receiptData.putString("processorLine1", refundResponse.receiptData.processorLine1)
    receiptData.putString("processorLine2", refundResponse.receiptData.processorLine2)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    receiptData.putString("footerLine2", refundResponse.receiptData.footerLine2)
    response.putMap("receiptData", receiptData)

    response.putString("reference", refundResponse.reference)
    response.putDouble("requestedAmount", refundResponse.requestedAmount ?: 0.00)
    response.putString("responseCode", refundResponse.responseCode)
    response.putInt("shift", refundResponse.shift ?: 0)
    response.putString("signatureData", refundResponse.signatureData)
    response.putBoolean("signatureRequired", refundResponse.signatureRequired)
    response.putInt("stan", refundResponse.stan)
    response.putString("storeID", refundResponse.storeID)
    response.putInt("table", refundResponse.table ?: 0)
    response.putDouble("taxAmount", refundResponse.taxAmount ?: 0.00)
    response.putString("terminalID", refundResponse.terminalID)
    response.putDouble("tipAmount", refundResponse.tipAmount ?: 0.00)
    response.putDouble("totalAmount", refundResponse.totalAmount)
    response.putString("transactionId", refundResponse.transactionId)
    response.putInt("transactionType", refundResponse.transactionType.value)
    response.putString("result", refundResponse.result.value)

    sendMessage("Refund Operation - Response Result: " + response.getString("result"))
    sendMessage("Refund Operation - Response Approval: " + response.getString("approval"))
    sendMessage("Refund Operation - Response Transaction ID: " + response.getString("transactionId"))

    return response
  }

  override fun log(log: String) {
    this.sendMessage("Terminal Log: $log")
    Log.d("ReactNativeMiuraSPA", log)
  }

  override fun notify(event: BaseEvent) {
    Log.d("ReactNativeMiuraSPA", event.result.toString())
    val eventResult = event.result.toString()
    this.sendTerminalEvent(eventResult)
  }
}
