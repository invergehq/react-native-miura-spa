import { NativeModules } from 'react-native';

type MiuraSpaType = {
  searchBluetooth(): Promise<
    {
      id: string;
      name: string;
    }[]
  >;
  initialize(paramFile: string): Promise<number>;
  getInitialized(): Promise<boolean>;
  connectBluetooth(id: string): Promise<boolean>;
  getTestMode(): Promise<boolean>;
  setTestMode(testMode: boolean): Promise<boolean>;
  displayMessage(id: string): Promise<boolean>;
  saleOperation(
    entryMode: 'manual' | 'swipe' | 'contact' | 'contactless',
    transaction: {
      amount: number;
      cashback: number;
      tip: number;
    },
    manualCard:
      | undefined
      | {
          cardPresent: boolean;
          address: string;
          postcode: string;
        }
  ): Promise<boolean>;
  refundOperation(
    entryMode: 'manual' | 'swipe' | 'contact' | 'contactless',
    refund: {
      amount: number;
      originalTransactionId: string;
    },
    manualCard:
      | undefined
      | {
          cardPresent: boolean;
          address: string;
          postcode: string;
        }
  ): Promise<boolean>;
};

const { MiuraSpa } = NativeModules;

export default MiuraSpa as MiuraSpaType;
